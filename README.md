# User input

hjkl: movement (vim motion)
(h: left, l: right, k: up, j: down)

yunb: diagonal movement
(y: NW, u: NE, n: SE, b: SW)

g: pick an item on the ground (when on it)
i: open the inventory
d: drop an item on the ground

esc: save the game and open the game menu
period: try go on the next level (should be on a '>')
space: skip turn
