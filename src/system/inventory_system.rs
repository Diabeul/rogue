use std::fmt::format;

use crate::{
    gamelog::GameLog, AreaOfEffect, CombatStats, Confusion, Consumable, InBackpack, InflictsDamage,
    Map, Name, Position, ProvidesHealing, SufferDamage, WantsToDropItem, WantsToPickupItem,
    WantsToUseItem,
};
use rltk::field_of_view;
use specs::prelude::*;

pub struct ItemCollectionSystem {}

impl<'a> System<'a> for ItemCollectionSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, Entity>,
        ReadStorage<'a, Name>,
        WriteExpect<'a, GameLog>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, WantsToPickupItem>,
        WriteStorage<'a, InBackpack>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (player_entity, names, mut gamelog, mut positions, mut wants_pickup, mut backpack) =
            data;

        for pickup in wants_pickup.join() {
            positions.remove(pickup.item);
            backpack
                .insert(
                    pickup.item,
                    InBackpack {
                        owner: pickup.collected_by,
                    },
                )
                .expect("Unable to insert backpack entry");

            if pickup.collected_by == *player_entity {
                gamelog.log(format!(
                    "You pick up the {}.",
                    names.get(pickup.item).unwrap().name
                ));
            }
        }

        wants_pickup.clear();
    }
}

pub struct ItemUseSystem {}

impl<'a> System<'a> for ItemUseSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Map>,
        ReadExpect<'a, Entity>,
        ReadStorage<'a, Name>,
        ReadStorage<'a, Consumable>,
        ReadStorage<'a, ProvidesHealing>,
        ReadStorage<'a, InflictsDamage>,
        ReadStorage<'a, AreaOfEffect>,
        WriteExpect<'a, GameLog>,
        WriteStorage<'a, WantsToUseItem>,
        WriteStorage<'a, SufferDamage>,
        WriteStorage<'a, CombatStats>,
        WriteStorage<'a, Confusion>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            map,
            player_entity,
            names,
            consumables,
            healing,
            inflict_damage,
            aoe,
            mut gamelog,
            mut wants_use,
            mut suffer_damage,
            mut stats,
            mut confused,
        ) = data;

        for (entity, useitem) in (&entities, &wants_use).join() {
            let mut used_item = true;

            // Targetting
            let mut targets: Vec<Entity> = Vec::new();
            match useitem.target {
                None => targets.push(*player_entity),
                Some(target) => {
                    if let Some(area) = aoe.get(useitem.item) {
                        // AoE
                        field_of_view(target, area.radius, &*map)
                            .iter()
                            .filter(|p| {
                                p.x > 0 && p.x < map.width - 1 && p.y > 0 && p.y < map.height - 1
                            })
                            .map(|tile| map.to_idx(tile.x, tile.y))
                            .for_each(|i| {
                                map.tile_content[i]
                                    .iter()
                                    .for_each(|mob| targets.push(*mob))
                            });
                    } else {
                        // Single target in tile
                        map.tile_content[map.to_idx(target.x, target.y)]
                            .iter()
                            .for_each(|mob| targets.push(*mob));
                    }
                }
            }

            // If it heals, apply healing
            if let Some(healer) = healing.get(useitem.item) {
                used_item = false;
                for target in targets.iter() {
                    let stats = stats.get_mut(*target);
                    if let Some(stats) = stats {
                        stats.hp = i32::min(stats.max_hp, stats.hp + healer.heal_amount);
                        if entity == *player_entity {
                            gamelog.log(format!(
                                "You drink the {}, healing {} hp.",
                                names.get(useitem.item).unwrap().name,
                                healer.heal_amount
                            ))
                        }
                        used_item = true;
                    }
                }
            }

            // If it inflicts damage, apply it to the target cell
            if let Some(damage) = inflict_damage.get(useitem.item) {
                used_item = false;
                for mob in targets.iter() {
                    SufferDamage::new_damage(&mut suffer_damage, *mob, damage.damage);
                    if entity == *player_entity {
                        let mob_name = names.get(*mob).unwrap();
                        let item_name = names.get(useitem.item).unwrap();
                        gamelog.log(format!(
                            "You use {} on {}, inflicting {} hp.",
                            item_name.name, mob_name.name, damage.damage
                        ));
                    }
                    used_item = true;
                }
            }

            // Can it pass along confusion? Not the use of Scoprs to escape the borrow checker!
            // let mut add_confusion = Vec::new();
            if let Some(confusion) = confused.get(useitem.item) {
                used_item = false;

                let mut mob_confusions = Vec::new();

                for mob in targets.iter() {
                    mob_confusions.push((*mob, confusion.turns));
                }

                for (mob, turns) in mob_confusions {
                    confused
                        .insert(mob, Confusion { turns })
                        .expect("Unable to insert status");

                    if entity == *player_entity {
                        let mob_name = names.get(mob).unwrap();
                        let item_name = names.get(useitem.item).unwrap();
                        gamelog.log(format!(
                            "You use {} on {}, confusing them.",
                            item_name.name, mob_name.name
                        ));
                    }

                    used_item = true;
                }
            }

            // If its a consumable and used, we delete it
            if consumables.get(useitem.item).is_some() && used_item {
                entities.delete(useitem.item).expect("Unable to delete");
            }
        }

        wants_use.clear();
    }
}

pub struct ItemDropSystem {}

impl<'a> System<'a> for ItemDropSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Entity>,
        ReadStorage<'a, Name>,
        WriteExpect<'a, GameLog>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, WantsToDropItem>,
        WriteStorage<'a, InBackpack>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, player_entity, names, mut log, mut positions, mut wants_drop, mut backpack) =
            data;

        for (entity, drop) in (&entities, &wants_drop).join() {
            let dropper_pos = positions.get(entity).unwrap().clone();
            positions
                .insert(drop.item, dropper_pos)
                .expect("Unable to insert position");
            backpack.remove(drop.item);

            if entity == *player_entity {
                log.log(format!(
                    "You drop the {}.",
                    names.get(drop.item).unwrap().name
                ));
            }
        }

        wants_drop.clear();
    }
}
