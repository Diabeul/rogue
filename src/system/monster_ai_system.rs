use crate::{Confusion, Map, Monster, Position, RunState, Viewshed, WantsToMelee};
use rltk::{a_star_search, Point};
use specs::prelude::*;

pub struct MonsterAI {}

impl<'a> System<'a> for MonsterAI {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Point>,
        ReadExpect<'a, Entity>,
        ReadExpect<'a, RunState>,
        ReadStorage<'a, Monster>,
        WriteExpect<'a, Map>,
        WriteStorage<'a, Viewshed>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, WantsToMelee>,
        WriteStorage<'a, Confusion>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            player_pos,
            player_entity,
            runstate,
            monster,
            mut map,
            mut viewshed,
            mut position,
            mut wants_to_melee,
            mut confused,
        ) = data;

        if *runstate != RunState::MonsterTurn {
            return;
        }

        for (entity, mut viewshed, _monster, mut pos) in
            (&entities, &mut viewshed, &monster, &mut position).join()
        {
            if let Some(i_am_confused) = confused.get_mut(entity) {
                i_am_confused.turns -= 1;
                if i_am_confused.turns < 1 {
                    confused.remove(entity);
                }
                continue;
            }

            let distance =
                rltk::DistanceAlg::Pythagoras.distance2d(Point::new(pos.x, pos.y), *player_pos);
            if distance < 1.5 {
                wants_to_melee
                    .insert(
                        entity,
                        WantsToMelee {
                            target: *player_entity,
                        },
                    )
                    .expect("Unable to insert attack");
            } else if viewshed.visible_tiles.contains(&*player_pos) {
                // Path to player
                let path = a_star_search(
                    map.to_idx(pos.x, pos.y),
                    map.to_idx(player_pos.x, player_pos.y),
                    &*map,
                );
                if path.success && path.steps.len() > 1 {
                    let mut i = map.to_idx(pos.x, pos.y);
                    map.blocked[i] = false;

                    pos.x = path.steps[1] as i32 % map.width;
                    pos.y = path.steps[1] as i32 / map.width;
                    i = map.to_idx(pos.x, pos.y);
                    map.blocked[i] = true;

                    viewshed.dirty = true;
                }
            }
        }
    }
}
