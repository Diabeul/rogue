pub struct GameLog {
    pub entries: Vec<String>,
}

impl GameLog {
    pub fn log(&mut self, entry: String) {
        self.entries.push(entry);
    }
}
