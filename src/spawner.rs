use crate::{
    AreaOfEffect, BlocksTile, CombatStats, Confusion, Consumable, InflictsDamage, Item, Monster,
    Name, Player, Position, ProvidesHealing, Ranged, Rect, Renderable, SerializeMe, Viewshed,
    MAPWIDTH,
};
use rltk::{to_cp437, RandomNumberGenerator, BLACK, CYAN, MAGENTA, ORANGE, PINK, RED, RGB, YELLOW};
use specs::prelude::*;
use specs::saveload::{MarkedBuilder, SimpleMarker};

const MAX_MONSTERS: i32 = 3; //4
const MAX_ITEMS: i32 = 5;

/// Fills a room with stuff
pub fn spawn_room(ecs: &mut World, room: &Rect) {
    let monster_spawn_points: Vec<usize> = generate_spawn_points(ecs, room, MAX_MONSTERS);
    let item_spawn_points: Vec<usize> = generate_spawn_points(ecs, room, MAX_ITEMS);

    for i in monster_spawn_points.iter() {
        let x = *i % MAPWIDTH;
        let y = *i / MAPWIDTH;
        random_monster(ecs, x as i32, y as i32);
    }

    for i in item_spawn_points.iter() {
        let x = *i % MAPWIDTH;
        let y = *i / MAPWIDTH;
        random_item(ecs, x as i32, y as i32);
    }
}

fn generate_spawn_points(ecs: &mut World, room: &Rect, max_spawn: i32) -> Vec<usize> {
    let mut spawn_points: Vec<usize> = Vec::new();

    let mut rng = ecs.write_resource::<RandomNumberGenerator>();

    for _ in 0..(rng.roll_dice(1, max_spawn + 2) - 3) {
        let mut added = false;
        while !added {
            let x = (room.x1 + rng.roll_dice(1, i32::abs(room.x2 - room.x1))) as usize;
            let y = (room.y1 + rng.roll_dice(1, i32::abs(room.y2 - room.y1))) as usize;
            let i = (y * MAPWIDTH) + x;
            if !spawn_points.contains(&i) {
                spawn_points.push(i);
                added = true;
            }
        }
    }

    spawn_points
}

/// Spawns the player and returns his/her entity object.
pub fn player(ecs: &mut World, player_x: i32, player_y: i32) -> Entity {
    ecs.create_entity()
        .with(Player {})
        .with(Name {
            name: "Player".to_string(),
        })
        .with(Position {
            x: player_x,
            y: player_y,
        })
        .with(Renderable {
            glyph: to_cp437('@'),
            fg: RGB::named(YELLOW),
            bg: RGB::named(BLACK),
            render_order: 0,
        })
        .with(CombatStats {
            max_hp: 30,
            hp: 30,
            defense: 2,
            power: 5,
        })
        .with(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build()
}

/// Spawns a random monster at a given location
fn random_monster(ecs: &mut World, x: i32, y: i32) {
    let roll: i32;
    {
        let mut rng = ecs.write_resource::<RandomNumberGenerator>();
        roll = rng.roll_dice(1, 2);
    }
    match roll {
        1 => orc(ecs, x, y),
        _ => goblin(ecs, x, y),
    }
}

fn orc(ecs: &mut World, x: i32, y: i32) {
    monster(ecs, x, y, to_cp437('o'), "Orc");
}
fn goblin(ecs: &mut World, x: i32, y: i32) {
    monster(ecs, x, y, to_cp437('g'), "Goblin");
}

fn monster<S: ToString>(ecs: &mut World, x: i32, y: i32, glyph: rltk::FontCharType, name: S) {
    ecs.create_entity()
        .with(Monster {})
        .with(Name {
            name: name.to_string(),
        })
        .with(BlocksTile {})
        .with(Position { x, y })
        .with(Renderable {
            glyph,
            fg: RGB::named(RED),
            bg: RGB::named(BLACK),
            render_order: 1,
        })
        .with(CombatStats {
            max_hp: 16,
            hp: 16,
            defense: 1,
            power: 4,
        })
        .with(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();
}

fn random_item(ecs: &mut World, x: i32, y: i32) {
    let roll: i32;
    {
        let mut rng = ecs.write_resource::<RandomNumberGenerator>();
        roll = rng.roll_dice(1, 4);
    }
    match roll {
        1 => magic_missile_scroll(ecs, x, y),
        2 => fireball_scroll(ecs, x, y),
        3 => confusion_scroll(ecs, x, y),
        _ => health_potion(ecs, x, y),
    }
}

fn health_potion(ecs: &mut World, x: i32, y: i32) {
    ecs.create_entity()
        .with(Item {})
        .with(Name {
            name: "Health Potion".to_string(),
        })
        .with(Consumable {})
        .with(ProvidesHealing { heal_amount: 8 })
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(';'),
            fg: RGB::named(MAGENTA),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();
}

fn magic_missile_scroll(ecs: &mut World, x: i32, y: i32) {
    ecs.create_entity()
        .with(Item {})
        .with(Name {
            name: "Magic Missile Scroll".to_string(),
        })
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(InflictsDamage { damage: 8 })
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(CYAN),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();
}

fn fireball_scroll(ecs: &mut World, x: i32, y: i32) {
    ecs.create_entity()
        .with(Item {})
        .with(Name {
            name: "Fireball Scroll".to_string(),
        })
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(InflictsDamage { damage: 8 })
        .with(AreaOfEffect { radius: 3 })
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(ORANGE),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();
}

fn confusion_scroll(ecs: &mut World, x: i32, y: i32) {
    ecs.create_entity()
        .with(Item {})
        .with(Name {
            name: "Confusion Scroll".to_string(),
        })
        .with(Consumable {})
        .with(Ranged { range: 6 })
        .with(Confusion { turns: 4 })
        .with(Position { x, y })
        .with(Renderable {
            glyph: to_cp437(')'),
            fg: RGB::named(PINK),
            bg: RGB::named(BLACK),
            render_order: 2,
        })
        .marked::<SimpleMarker<SerializeMe>>()
        .build();
}
